using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Diagnostics.HealthChecks;

// ReSharper disable MemberCanBePrivate.Global --> Need to be public for JSON Serialization
// ReSharper disable UnusedAutoPropertyAccessor.Global --> Need to be accessed for JSON Serialization
namespace EcoLogic.HealthChecks
{
    public class HealthResponseDto
    {
        public HealthResponseDto(HealthReport result)
        {
            Status = result.Status.ToString();
            Duration = result.TotalDuration.TotalSeconds.ToString("0:0.00");
            Checks = result.Entries
                .Select(item => new HealthResponseDto(item))
                .ToList();
        }

        private HealthResponseDto(KeyValuePair<string, HealthReportEntry> item)
        {
            Name = item.Key;
            Status = item.Value.Status.ToString();
            Duration = item.Value.Duration.TotalSeconds.ToString("0:0.00");
        }

        public string Name { get; }
        public string Status { get; }
        public string Duration { get; }
        public IEnumerable<HealthResponseDto> Checks { get; }
    }
}