using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MailKit;
using MailKit.Net.Imap;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace EcoLogic.HealthChecks.Sendgrid
{
    public class SendgridHealthCheck : IHealthCheck
    {
        private readonly SendgridHealthCheckOptions _options;

        private readonly IReadOnlyDictionary<string, object> _healthCheckData;

        public SendgridHealthCheck(SendgridHealthCheckOptions options)
        {
            _options = options;
            _healthCheckData = new Dictionary<string, object> {{"Email", _options.Email}};
        }

        public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            try
            {
                var recentMailsContactForm = 0;
                var recentMailsApplicationForm = 0;

                using (var client = new ImapClient())
                {
                    client.Connect(_options.Server, _options.Port, true, cancellationToken);
                    client.Authenticate(_options.Email, _options.Password, cancellationToken);

                    var inbox = client.Inbox;
                    inbox.Open(FolderAccess.ReadWrite);

                    var mailInfos = inbox.Fetch(0, -1,
                        MessageSummaryItems.UniqueId | MessageSummaryItems.Size | MessageSummaryItems.Flags);

                    foreach (var mailInfo in mailInfos)
                    {
                        var message = inbox.GetMessage(mailInfo.UniqueId);

                        if (message.Subject.Contains("Bewerbung für") &&
                            message.Date > DateTimeOffset.UtcNow.AddDays(-2))
                        {
                            recentMailsApplicationForm++;
                        }

                        if (message.Subject.Contains("Kontaktanfrage über") &&
                            message.Date > DateTimeOffset.UtcNow.AddDays(-2))
                        {
                            recentMailsContactForm++;
                        }

                        // Delete mails older than 31 days
                        if (message.Date < DateTimeOffset.UtcNow.AddDays(-31))
                        {
                            inbox.AddFlags(mailInfo.UniqueId, MessageFlags.Deleted, true);
                        }
                    }

                    inbox.Expunge(cancellationToken);

                    client.Disconnect(true, cancellationToken);
                }

                if (recentMailsApplicationForm > 0 && recentMailsContactForm > 0)
                {
                    return Task.FromResult(HealthCheckResult.Healthy());
                }

                return Task.FromResult(HealthCheckResult.Degraded("Mails not found"));
            }
            catch (Exception e)
            {
                return Task.FromResult(HealthCheckResult.Degraded("Sendgrid Mail not working", e, _healthCheckData));
            }
        }
    }
}