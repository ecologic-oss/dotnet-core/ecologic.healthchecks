using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace EcoLogic.HealthChecks.Sendgrid
{
    public static class SendgridHealthCheckBuilderExtensions
    {
        private const string Name = "sendgrid";

        public static IHealthChecksBuilder AddSendgrid(
            this IHealthChecksBuilder builder,
            Action<SendgridHealthCheckOptions> setup,
            string name = null,
            HealthStatus? failureStatus = null,
            IEnumerable<string> tags = null)
        {
            var sendgridOptions = new SendgridHealthCheckOptions();
            setup?.Invoke(sendgridOptions);
            return builder.Add(new HealthCheckRegistration(name ?? Name, sp => (IHealthCheck) new SendgridHealthCheck(sendgridOptions), failureStatus, tags));
        }
    }
}