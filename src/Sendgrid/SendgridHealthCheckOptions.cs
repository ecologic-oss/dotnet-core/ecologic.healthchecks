namespace EcoLogic.HealthChecks.Sendgrid
{
    public class SendgridHealthCheckOptions
    {
        public string EmailFrom { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Server { get; set; }
        public int Port { get; set; }
    }
}