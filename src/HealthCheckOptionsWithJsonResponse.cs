using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Text.Json;

namespace EcoLogic.HealthChecks
{
    public class HealthCheckOptionsWithJsonResponse : HealthCheckOptions
    {
        public HealthCheckOptionsWithJsonResponse()
        {
            ResponseWriter = WriteHealthCheckJsonResponse;
        }

        private static Task WriteHealthCheckJsonResponse(HttpContext httpContext, HealthReport result)
        {
            httpContext.Response.ContentType = "application/json";
            var options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                IgnoreNullValues = true
            };
            return httpContext.Response.WriteAsync(JsonSerializer.Serialize(new HealthResponseDto(result), options));
        }
    }
}