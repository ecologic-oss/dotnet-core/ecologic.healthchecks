# EcoLogic.HealthChecks - A set of ASP .NET Core Health Checks

**EcoLogic.HealthChecks** provides a set of ASP .NET Core Health Checks and global the possibility to return status checks as JSON.

**Available checks**

-   Sendgrid

## Return Status Checks as JSON

Use `HealthCheckOptionsWithJsonResponse` instead of `HealthCheckOptions` to make a HealthCheck return a nicely formated JSON response instead of some text content-type.


**Startup.cs**

```csharp
app.UseHealthChecks(
    "/health/ready",
    new HealthCheckOptionsWithJsonResponse() // Instead of 'new HealthCheckOptions()'
    {
        Predicate = check => check.Tags.Contains("ready")
    });
```

**Example Response (JSON)**

```json
{
  "status": "Healthy",
  "duration": "0:0.04",
  "checks": [
    {
      "name": "Hangfire",
      "status": "Healthy",
      "duration": "0:0.01"
    },
    {
      "name": "Hangfire Database",
      "status": "Healthy",
      "duration": "0:0.01"
    },
    {
      "name": "App Database",
      "status": "Healthy",
      "duration": "0:0.02"
    },
     {
      "name": "Sendgrid",
      "status": "Healthy",
      "duration": "0:0.04"
    }
  ]
}
```


## Health Checks

### Sendgrid

**Startup.cs**

```csharp
services.AddHealthChecks()
        .AddSendgrid(
            options => Configuration.GetSection("Sendgrid:HealthCheck").Get<SendgridHealthCheckOptions>(),
            name: "Sendgrid Health Check",
            tags: new string[] {"live"}
        );
```
