using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Xunit;
using Xunit.Abstractions;

namespace EcoLogic.HealthChecks.Tests
{
    public class HealthCheckOptionsWithJsonResponseTests
    {
        private readonly ITestOutputHelper _testOutputHelper;

        public HealthCheckOptionsWithJsonResponseTests(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
        }

        [Fact]
        public async void HealthCheckOptionsWithJsonResponse_ReturnsContentType_Json()
        {
            // Arrange
            var healthCheckOptionsWithJsonResponse = new HealthCheckOptionsWithJsonResponse();
            var httpContext = new DefaultHttpContext();
            var healthReport = new HealthReport(new Dictionary<string, HealthReportEntry>(), TimeSpan.FromMilliseconds(311));

            // Act
            await healthCheckOptionsWithJsonResponse.ResponseWriter(httpContext, healthReport);

            // Assert
            Assert.Equal("application/json", httpContext.Response.ContentType);
        }

        [Fact]
        public async void HealthCheckOptionsWithJsonResponse_ReturnsCorrectlyFormated_JsonBody()
        {
            // Arrange
            var healthCheckOptionsWithJsonResponse = new HealthCheckOptionsWithJsonResponse();
            var httpContext = new DefaultHttpContext();
            httpContext.Response.Body = new MemoryStream();
            var healthReport = new HealthReport(
                new Dictionary<string, HealthReportEntry>
                {
                    {"Sendgrid", new HealthReportEntry(HealthStatus.Healthy, "", TimeSpan.FromMilliseconds(311), null, new Dictionary<string, object>())}
                },
                TimeSpan.FromMilliseconds(311)
            );
            const string expectedJsonAnswer = "{\"status\":\"Healthy\",\"duration\":\"0:0.31\",\"checks\":[{\"name\":\"Sendgrid\",\"status\":\"Healthy\",\"duration\":\"0:0.31\"}]}";

                // Act
            await healthCheckOptionsWithJsonResponse.ResponseWriter(httpContext, healthReport);
            httpContext.Response.Body.Position = 0;
            var reader = new StreamReader(httpContext.Response.Body);
            var jsonBody = reader.ReadToEnd();
            var flatJsonBody = Regex.Replace(jsonBody, @"\t|\n|\r", "").Trim();

            _testOutputHelper.WriteLine($"JSON Body:\n\n{jsonBody}");

            // Assert
            Assert.Equal(expectedJsonAnswer, flatJsonBody);
        }
    }
}